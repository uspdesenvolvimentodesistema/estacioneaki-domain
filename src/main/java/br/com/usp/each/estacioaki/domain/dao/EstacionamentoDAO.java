package br.com.usp.each.estacioaki.domain.dao;

import java.util.List;

import javax.persistence.EntityManager;

import antlr.CppCodeGenerator;
import br.com.usp.each.estacioaki.domain.Estacionamento;

public class EstacionamentoDAO extends BaseDAO<Estacionamento>{
	
	public EstacionamentoDAO(){
		super();
		setClazz(Estacionamento.class);
	}
	
	 @Override
	public void setClazz(Class<Estacionamento> clazz) {
		// TODO Auto-generated method stub
		super.setClazz(Estacionamento.class);
		
	}
	 
	 public boolean jaExiste(String cnpj,EntityManager entityManager){
			 String query = "from Estacionamento c where c.CNPJ = :valor ";
			 List<Object> obj = entityManager.createQuery(query).setParameter("valor",cnpj).getResultList();
		         
			 if(obj==null || obj.isEmpty()){
				 return false;
			 }
			 return true;
		 
	 }
	 
	 public Estacionamento Login(String login,String senha, EntityManager entityManager){
		 String query = "from Estacionamento c where c.login = :valor and c.senha = :valor2 ";
		 List<Object> obj = entityManager.createQuery(query).setParameter("valor",login).setParameter("valor2", senha).getResultList();
	         
		 if(obj==null || obj.isEmpty()){
			 return null;
		 }
		 return (Estacionamento)obj.get(0);
	 }
		 public Estacionamento findByCnpj(String cnpj ,EntityManager entityManager){
			 String query = "from Estacionamento c where c.CNPJ = :valor ";
			 List<Object> obj = entityManager.createQuery(query).setParameter("valor",cnpj).getResultList();
		         
			 if(obj==null || obj.isEmpty()){
				 return null;
			 }
			 return (Estacionamento)obj.get(0);
	 
 }


}
