package br.com.usp.each.estacioaki.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class Marca implements Dominio{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String marca;
	@OneToMany(fetch = FetchType.EAGER)
	private List<Modelo> modelo;
	
	public String getMarca() {
		return marca;
	}
	public List<Modelo> getModelo() {
		if(modelo ==null){
			modelo = new ArrayList<Modelo>();
		}
		return modelo;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	public void setModelo(List<Modelo> modelo) {
		this.modelo = modelo;
	}
	
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
}
