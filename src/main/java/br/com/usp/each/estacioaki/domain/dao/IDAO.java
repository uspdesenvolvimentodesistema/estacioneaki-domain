package br.com.usp.each.estacioaki.domain.dao;

import java.util.List;

import javax.persistence.EntityManager;

public interface IDAO<T> {

	
    public void save(T entity,EntityManager entityManager);
    
    public void delete(T entity,EntityManager entityManager);
    
    public void update(T entity,EntityManager entityManager);
    
    public T find(Long entityId,EntityManager entityManager);
    
    public List<T> findAll(EntityManager entityManager);
	
}
