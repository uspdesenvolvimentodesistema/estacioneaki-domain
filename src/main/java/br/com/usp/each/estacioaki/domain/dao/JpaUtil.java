package br.com.usp.each.estacioaki.domain.dao;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaUtil {  
    
	  
    private static EntityManagerFactory factory;  
      
    static {  
        factory = Persistence.createEntityManagerFactory("estacionaaki");          
  
    }  
      
    public static EntityManager getManager () {  
          
        return factory.createEntityManager();  
          
    }
    
    public static void finishTransaction(final EntityManager entityManager) {
    	if(entityManager !=null && entityManager.isOpen()){
    		entityManager.getTransaction().commit();
			entityManager.close();
    	}
	}
    
    public static void rollbackTransaction(final EntityManager entityManager) {
		entityManager.getTransaction().rollback();
		entityManager.close();
	}
	public static EntityManager startTransaction(EntityManager entityManager) {
		entityManager = JpaUtil.getManager();
		entityManager.getTransaction().begin();
		return entityManager;
	}
} 