package br.com.usp.each.estacioaki.domain.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.usp.each.estacioaki.domain.Dominio;

public class BaseDAO<T> implements IDAO<T>{
     private Class<T> clazz;
     public BaseDAO(){
    	  
     }
             
     /** 
      *{@inheritDoc}
      */
     public void save(T entity, EntityManager entityManager) {
             entityManager.persist(entity);
     }

     /** 
      *{@inheritDoc}
      */
     public void delete(T entity, EntityManager entityManager) {
             T delEntity = find(((Dominio)entity).getId(),entityManager);
             entityManager.remove(delEntity);
     }

     /** 
      *{@inheritDoc}
      */
     public void update(T entity, EntityManager entityManager) {
             entityManager.merge(entity);
     }

     /** 
      *{@inheritDoc}
      */
     public T find(Long entityId, EntityManager entityManager) {
    	
             T entity = (T) entityManager.find(clazz, entityId);
            
             return entity;
     }

     /** 
      *{@inheritDoc}
      */
     public List<T> findAll(EntityManager entityManager) {
             List<T> list = entityManager.createQuery("from " + clazz.getName(), clazz).getResultList();
             return new ArrayList<T>(list);
     }
     
//     /** 
//      *{@inheritDoc}
//      */
//     public void setEntityManager(EntityManager entityManager) {
//             
//    	 	this.entityManager = entityManager;
//     }
//     
//
//     /** 
//      *{@inheritDoc}
//      */
//     public EntityManager getEntityManager() {
//             return entityManager;
//     }
//     
//     /** 
//      *{@inheritDoc}
//      */
     public void setClazz(Class<T> clazz) {
             this.clazz = clazz;
     }
//     
//     /** 
//      * Inicia uma transação
//      */
//     protected void startTransaction(){
//    	 entityManager.getTransaction().begin();
//    	 
//     }
//     
//     /** 
//      * Commita a transação e fecha o entityManager
//      */
//     protected void commitTransaction(){
//             entityManager.getTransaction().commit();
//             entityManager.close();
//     }
}
