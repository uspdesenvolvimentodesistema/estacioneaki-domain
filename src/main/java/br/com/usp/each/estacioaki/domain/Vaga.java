package br.com.usp.each.estacioaki.domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class Vaga implements Dominio{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String coordenadas;
	private Integer status;
	@Enumerated(EnumType.ORDINAL)
	private Tamanho tamanho;
	
	@Enumerated(EnumType.ORDINAL)
	private StatusVaga statusVaga;
	
	
	public StatusVaga getStatusVaga() {
		return statusVaga;
	}
	
	public void setStatusVaga(StatusVaga statusVaga) {
		this.statusVaga = statusVaga;
	}
	
	public Tamanho getTamanho() {
		return tamanho;
	}
	
	public void setTamanho(Tamanho tamanho) {
		this.tamanho = tamanho;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getCoordenadas() {
		return coordenadas;
	}

	public void setCoordenadas(String coordenadas) {
		this.coordenadas = coordenadas;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	
}
