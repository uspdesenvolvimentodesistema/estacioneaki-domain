package br.com.usp.each.estacioaki.domain;

public enum TipoAluguel {

	ALUGUEL(1), RESERVA(2);

	private Integer tipo;

	TipoAluguel(Integer tamanho) {
		this.tipo = tamanho;
	}

	public Integer getTamanho() {
		return this.tipo;
	}
}
