package br.com.usp.each.estacioaki.domain;

public enum StatusVaga {

	DISPONIVEL(0),OCUPADO(1),RESERVADO(2);
	
	private Integer tamanho;

	StatusVaga(Integer tamanho) {
		this.tamanho = tamanho;
	}

	public Integer getTamanho() {
		return this.tamanho;
	}
}