package br.com.usp.each.estacioaki.domain.dao;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.usp.each.estacioaki.domain.Marca;
import br.com.usp.each.estacioaki.domain.Modelo;

public class MarcaDAO extends BaseDAO<Marca>{
	
	public MarcaDAO(){
		super();
		setClazz(Marca.class);
	}
	 @Override
	public void setClazz(Class<Marca> clazz) {
		// TODO Auto-generated method stub
		super.setClazz(Marca.class);
		
	}
	 
	@Override
	public Marca find(Long entityId, EntityManager entityManager) {
		// TODO Auto-generated method stub
		Marca m =  super.find(entityId, entityManager);
		List<Modelo> modelos = new ArrayList<Modelo>();
		ModeloDAO modeloDAO = new ModeloDAO();
		for(Modelo mod : m.getModelo() ){
			modelos.add(modeloDAO.find(mod.getId(), entityManager));
		}
		
		m.setModelo(modelos);
		
		return m;
	}
	
	@Override
	public List<Marca> findAll(EntityManager entityManager) {
		// TODO Auto-generated method stub
		List<Marca> marcasOn=  super.findAll(entityManager);
		List<Marca> marcas=  new ArrayList<Marca>();
		for(Marca mOn : marcasOn){
			Marca m = find(mOn.getId(), entityManager);
			marcas.add(m);
		}
		
		return marcas;
		
	}

}
