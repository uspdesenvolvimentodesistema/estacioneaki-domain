package br.com.usp.each.estacioaki.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class Veiculo implements Dominio, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String placa;
	private String cor;
	@OneToOne
	private Marca marca;
	@OneToOne
	private Modelo modelo;
	
//	private String marca;
//	private String modelo;
	@Enumerated(EnumType.ORDINAL)
	private Tamanho tipo;
	
//	public String getModelo() {
//		return modelo;
//	}
//	public void setModelo(String modelo) {
//		this.modelo = modelo;
//	}
//	
//	public String getMarca() {
//		return marca;
//	}
//	
//	public void setMarca(String marca) {
//		this.marca = marca;
//	}
	
	
	public Marca getMarca() {
		return marca;
	}
	
	public Modelo getModelo() {
		return modelo;
	}
	
	public void setMarca(Marca marca) {
		this.marca = marca;
	}
	
	public void setModelo(Modelo modelo) {
		this.modelo = modelo;
	}
	
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getCor() {
		return cor;
	}
	
	
	public void setCor(String cor) {
		this.cor = cor;
	}
	

	public Tamanho getTipo() {
		return tipo;
	}
	
	public void setTipo(Tamanho tipo) {
		this.tipo = tipo;
	}
	
	public String getPlaca() {
		return placa;
	}
	
	public void setPlaca(String placa) {
		this.placa = placa;
	}
}
