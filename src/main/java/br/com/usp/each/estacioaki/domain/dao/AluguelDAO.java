package br.com.usp.each.estacioaki.domain.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.usp.each.estacioaki.domain.Aluguel;
import br.com.usp.each.estacioaki.domain.Vaga;
import br.com.usp.each.estacioaki.domain.Veiculo;

public class AluguelDAO extends BaseDAO<Aluguel> {


	public AluguelDAO(){
		super();
		setClazz(Aluguel.class);
	}
	
	 @Override
	public void setClazz(Class<Aluguel> clazz) {
		// TODO Auto-generated method stub
		super.setClazz(Aluguel.class);
		
	}
	 
	 public Aluguel findByVeiculoPendente(Veiculo veiculo, EntityManager entityManager){
		 String query = "from Aluguel a where a.veiculo = :veiculo and a.dataHoraFinal is null";
		 List<Object> obj = entityManager.createQuery(query).setParameter("veiculo",veiculo).getResultList();
	         
		 if(obj==null || obj.isEmpty()){
			 return null;
		 }
		 return (Aluguel)obj.get(0);
	 
	 }
	 
	 public Aluguel findByVaga(Vaga vaga, EntityManager entityManager){
		 String query = "from Aluguel a where a.vaga = :vaga and a.dataHoraFinal is null";
		 List<Object> obj = entityManager.createQuery(query).setParameter("vaga",vaga).getResultList();
	         
		 if(obj==null || obj.isEmpty()){
			 return null;
		 }
		 return (Aluguel)obj.get(0);
	 
	 }
}
