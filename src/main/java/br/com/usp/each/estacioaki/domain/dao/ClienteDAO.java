package br.com.usp.each.estacioaki.domain.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.usp.each.estacioaki.domain.Cliente;
import br.com.usp.each.estacioaki.domain.Veiculo;


public class ClienteDAO extends BaseDAO<Cliente> {

	public ClienteDAO(){
		super();
		setClazz(Cliente.class);
	}
	
	 @Override
	public void setClazz(Class<Cliente> clazz) {
		// TODO Auto-generated method stub
		super.setClazz(Cliente.class);
		
	}
	 
	 public boolean jaExiste(String cpf,EntityManager entityManager){
			 Cliente cliente = new Cliente();
			 cliente.setCpf_cnpj(cpf);
			 String query = "from Cliente c where c.cpf_cnpj = :valor ";
			 List<Object> obj = entityManager.createQuery(query).setParameter("valor",cliente.getCpf_cnpj()).getResultList();
		         
			 if(obj==null || obj.isEmpty()){
				 return false;
			 }
			 return true;
		 
	 }
	 
	 public Cliente Login(String cpf,String senha, EntityManager entityManager){
		 String query = "from Cliente c where c.cpf_cnpj = :valor and c.senha =:valor2";
		 List<Object> obj = entityManager.createQuery(query).setParameter("valor",cpf).setParameter("valor2", senha).getResultList();
	         
		 if(obj==null || obj.isEmpty()){
			 return null;
		 }
		 return (Cliente)obj.get(0);
	 
	 }
	 
	 public Cliente findByVeiculo(Veiculo v , EntityManager entityManager){
		 String query = " from Cliente c inner join c.veiculos v where v.id = :veiculo";
		 List<Object> obj = entityManager.createQuery(query).setParameter("veiculo",v.getId()).getResultList();
	         
		 if(obj==null || obj.isEmpty()){
			 return null;
		 }
		 Object[] obj2 = (Object[])obj.get(0);
		 if(obj2.length<0 || obj2[0]==null){
			 return null;
		 }
		 return (Cliente)obj2[0];
	 
	 }

}
