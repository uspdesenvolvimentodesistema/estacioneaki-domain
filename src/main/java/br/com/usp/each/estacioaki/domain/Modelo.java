package br.com.usp.each.estacioaki.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class Modelo implements Dominio {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String modelo;
//	@OneToOne(fetch = FetchType.LAZY)
//	private Marca marca;
	
//	public Marca getMarca() {
//		return marca;
//	}
//	
//	public void setMarca(Marca marca) {
//		this.marca = marca;
//	}
	
	public String getModelo() {
		return modelo;
	}
	 
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
}
