package br.com.usp.each.estacioaki.domain;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
@Entity
public class Aluguel implements Dominio{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataHoraInicial;
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataHoraFinal;
	@OneToOne
	private Vaga vaga;
	@OneToOne
	private Veiculo veiculo;
	@OneToOne
	private Pagamento pagamento;
	@Enumerated(EnumType.ORDINAL)
	private TipoAluguel tipoAluguel;
	
	public TipoAluguel getTipoAluguel() {
		return tipoAluguel;
	}
	
	public void setTipoAluguel(TipoAluguel tipoAluguel) {
		if(tipoAluguel==TipoAluguel.ALUGUEL){
			vaga.setStatusVaga(StatusVaga.OCUPADO);
		}
		else if(tipoAluguel==TipoAluguel.RESERVA){
			vaga.setStatusVaga(StatusVaga.RESERVADO);
		}
		
		this.tipoAluguel = tipoAluguel;
	}
	public Pagamento getPagamento() {
		return pagamento;
	}
	
	public void setPagamento(Pagamento pagamento) {
		this.pagamento = pagamento;
	}
	
	
	public Calendar getDataHoraInicial() {
		return dataHoraInicial;
	}
	public void setDataHoraInicial(Calendar dataHoraInicial) {
		this.dataHoraInicial = dataHoraInicial;
	}
	public Calendar getDataHoraFinal() {
		return dataHoraFinal;
	}
	public void setDataHoraFinal(Calendar dataHoraFinal) {
		this.dataHoraFinal = dataHoraFinal;
	}
	public Vaga getVaga() {
		return vaga;
	}
	public void setVaga(Vaga vaga) {
		this.vaga = vaga;
	}
	public Veiculo getVeiculo() {
		return veiculo;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	
}
