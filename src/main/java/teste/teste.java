package teste;

import javax.persistence.EntityManager;

import br.com.usp.each.estacioaki.domain.Aluguel;
import br.com.usp.each.estacioaki.domain.Cliente;
import br.com.usp.each.estacioaki.domain.Estacionamento;
import br.com.usp.each.estacioaki.domain.Marca;
import br.com.usp.each.estacioaki.domain.Modelo;
import br.com.usp.each.estacioaki.domain.StatusVaga;
import br.com.usp.each.estacioaki.domain.Tamanho;
import br.com.usp.each.estacioaki.domain.Vaga;
import br.com.usp.each.estacioaki.domain.Veiculo;
import br.com.usp.each.estacioaki.domain.dao.AluguelDAO;
import br.com.usp.each.estacioaki.domain.dao.ClienteDAO;
import br.com.usp.each.estacioaki.domain.dao.EstacionamentoDAO;
import br.com.usp.each.estacioaki.domain.dao.JpaUtil;
import br.com.usp.each.estacioaki.domain.dao.MarcaDAO;
import br.com.usp.each.estacioaki.domain.dao.VagaDAO;
import br.com.usp.each.estacioaki.domain.dao.VeiculoDAO;

public class teste {

	public static void main(String[] args) {
		
//		System.out.println(new ClienteDAO().findAll(entityManager).size());
//		JpaUtil.finishTransaction(entityManager);
//		
//		Cliente c = new Cliente();
//		c.setCPF_CNPJ("11111111112");
//		c.setNome("Teste");
//		c.setStatus(1);
//		c.setTelefone("1198765432");
//		Veiculo v = new Veiculo();
//		v.setCor("azul");
//		v.setMarca("Ford");
//		v.setModelo("EcoSport");
//		v.setPlaca("ESD-0987");
//		v.setTipo(Tamanho.HATCH);
//		c.getVeiculos().add(v);
		EntityManager entityManager = null;
		entityManager = JpaUtil.startTransaction(entityManager);
//		System.out.println(entityManager.getTransaction().isActive());
//		new VeiculoDAO().save(v, entityManager);
//		new ClienteDAO().save(c, entityManager);
//		Cliente c = new Cliente();
		Veiculo v = new VeiculoDAO().find(12l, entityManager);
		Cliente c = new ClienteDAO().findByVeiculo(v, entityManager);
		
//		c = new AluguelDAO().findByVaga(v,entityManager);
//		Aluguel a = new AluguelDAO().findByVaga(v, entityManager);
//		if(a!=null){
//			new AluguelDAO().delete(a, entityManager);
		System.out.println(c);
		
		
//		Marca marca = new MarcaDAO().find(36l, entityManager);
//		for(Modelo m : marca.getModelo()){
//			System.out.println("Marca :"+marca.getMarca()+" Modelo: "+m.getModelo() );
//		}
//	
//		
//		Veiculo v = new VeiculoDAO().find(1l, entityManager);
//		
//		Cliente c = new ClienteDAO().find(15l, entityManager);
		JpaUtil.finishTransaction(entityManager);

	}
}
