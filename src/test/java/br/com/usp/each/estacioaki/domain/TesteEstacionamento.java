package br.com.usp.each.estacioaki.domain;

import static org.junit.Assert.*;

import org.junit.Test;

public class TesteEstacionamento {

	@Test
	public void testaddVagas() {
		Estacionamento esta = new Estacionamento();
		esta.getVagas().add(new Vaga());
		esta.getVagas().add(new Vaga());
		esta.getVagas().add(new Vaga());
		esta.getVagas().add(new Vaga());
		
		assertEquals(4, esta.getVagas().size());
	}

}
