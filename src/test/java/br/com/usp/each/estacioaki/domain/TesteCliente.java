package br.com.usp.each.estacioaki.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.junit.Test;

public class TesteCliente {

	@Test
	public void testmd5() {
		Cliente c =new Cliente();
		c.setSenha("testeJunit");
		assertFalse(c.getSenha().equals("testeJunit"));
	}

	
	@Test
	public void testmd5equal() throws NoSuchAlgorithmException {
		Cliente c =new Cliente();
		c.setSenha("testeJunit");
		
		String original = "testeJunit";
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(original.getBytes());
		byte[] digest = md.digest();
		StringBuffer sb = new StringBuffer();
		for (byte b : digest) {
			sb.append(String.format("%02x", b & 0xff));
		}
		sb.toString();
		assertTrue(c.getSenha().equals(sb.toString()));
	}
	
	@Test
	public void testmd5null(){
		Cliente c =new Cliente();
		c.setSenha("");
		assertTrue(c.getSenha().equals(""));
	}
	@Test
	public void testeaddVeiculos(){
		Cliente c =new Cliente();
		c.getVeiculos().add(new Veiculo());
		c.getVeiculos().add(new Veiculo());
		c.getVeiculos().add(new Veiculo());
		
		assertEquals(3, c.getVeiculos().size());
	}
}
