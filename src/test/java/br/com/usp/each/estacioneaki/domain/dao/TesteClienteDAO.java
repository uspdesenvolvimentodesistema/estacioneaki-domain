package br.com.usp.each.estacioneaki.domain.dao;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import br.com.usp.each.estacioaki.domain.dao.ClienteDAO;

public class TesteClienteDAO {

	private ClienteDAO clienteDAO ;
	
	@Before
	public void init(){
	clienteDAO = Mockito.mock(ClienteDAO.class);
	}
	
	@Test
	public void jaExisteSim(){

		Mockito.when(clienteDAO.jaExiste("1111111111", null)).thenReturn(true);
		assertTrue(clienteDAO.jaExiste("1111111111", null));
	}
	
	@Test
	public void jaExisteNao(){

		Mockito.when(clienteDAO.jaExiste("1111111112", null)).thenReturn(false);
		assertFalse(clienteDAO.jaExiste("1111111111", null));
	}


}
